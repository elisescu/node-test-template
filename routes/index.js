
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'My page' });
};

exports.default = function(req, res) {
  res.send("No page to display");
}

exports.login = function(req, res) {
  res.render('login');
}